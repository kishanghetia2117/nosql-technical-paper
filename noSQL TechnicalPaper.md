# **NoSQL Database performance research**

## **Abstract**

There is a wide range of NoSQL databases that have different advantages and performance levels at various stages. This paper studies a list of NoSQL databases, their performance, advantages, and disadvantages at each stage. Giving out an analysis of which NoSQL database we should use based on the type of data and features we need, as well as the possibility of performance improvement.

## **Introduction**

We always get confused while choosing between NoSQL or SQL databases. There is a possibility that although the SQL database is widely used by many organizations and has many use cases, we might encounter performance problems. We may need to consider using NoSQL and comparing their performance. We get search results with phrases like 'NoSQL is the future of databases', 'SQL is outdated', 'NoSQL is fast', etc. When it comes to selecting a NoSQL database, there are a lot of options available. Some are well-known, such as Cassandra, MongoDB, Hbase, etc. These can vary based on the type of data being stored and the features it offers. This results in varying levels of performance at various stages such as reading, writing, updating, and a combination of them. This makes it critical for us to make a performance analysis on each of the databases and find out what features and advantages it offers over the other.

## **What's RDBMS or SQL?**

SQL was created in the early 1970s at IBM. SQL stands for Structured Query Language. It's used for relational databases management systems (RDBMS). A SQL database is a collection of tables that stores a specific set of structured data. It allows us to perform CRUD operations and run administrative tasks on the database. Different types of SQL databases differ by a bit, but have almost the same features and adhere to the same principles.

## **What's NoSQL?**

NoSQL (“non-SQL” or “not only SQL”) databases were developed in the late 2000s. Data is stored in non-tabular structures format. Instead, they follow different structure formats based on their use case. Some of the most common types include wide column, key-value, document stores, and graph. Each NoSQL database is designed in one of these formats.

## **Why NoSQL?**

As NoSQL databases do not follow a strict schema, they can handle large volumes of structured, semi-structured, and unstructured data by scaling out better than SQL databases.

![scaling out image](images/scaleup2.png "scaling out image")

## **NoSQL: Data Modeling Example :**

Data in NoSQL is stored in a different format. One of the ways is as follows: stored in the object format.

![scaling out image](images/structure.png "scaling out image")

## **NoSQL database features :**

### 1. Flexible schemas and Non-relational

* NoSQL databases are either schema-free or have relaxed schemas.
* They do not follow the relational model.
* Definition of the schema for the data is unnecessary.
* Offers heterogeneous structures of data.
* Does not provide tables with flat fixed-column records.
* Doesn’t require object and relational mapping.
* Can iterate quickly and continuously integrate updated application features to provide value to your users faster.
  
### 2. Horizontal scaling and Distributed

* NoSQL databases allow you to scale-out horizontally
* Added cheaper, commodity servers whenever you need to.
* Multiple NoSQL databases can be executed in a distributed fashion
* Offers auto-scaling and fail-over capabilities
* Often ACID concept can be sacrificed for scalability and throughput
* Shared Nothing Architecture. This enables less coordination and higher distribution.

### 3. Fast queries due to the data model

* Queries in NoSQL databases can be faster than SQL databases.
* Data in NoSQL databases are typically stored in a way that is optimized for queries.
* Queries typically do not require joins, so the queries are very fast.

### 4. Ease of use for developers

* Some NoSQL databases map their data structures to popular programming languages.
* This allows developers to store their data in the same way that they use it in their application code.
* This mapping can allow developers to write less code, leading to faster development time and fewer bugs.

## **CAP Theorem**

CAP theorem states that it’s impossible to simultaneously provide more than two of the following three guarantees.

* **Consistency** The data should remain consistent even after the execution of an operation. I.e once data is written, any future read request should contain that data.

* **Availability** The database should always be available and responsive, without any downtime.

* **Partition Tolerance** The system should continue to function even if the communication among the servers is not stable. The servers can be partitioned into multiple groups which may not communicate with each other. If part of the database is unavailable, other parts are always unaffected.
  
![scaling out image](images/cap.png "scaling out image")

## **Eventual Consistency**

It stands to have copies of data on multiple machines to get high availability and scalability. Changes made to any data item on one machine have to be propagated to other replicas.

Data replication may not be quick as some copies will be updated immediately. Others become consistent over time. Hence, the name eventual consistency.

BASE: **B**asically **A**vailable, **S**oft state, **E**ventual consistency.

* Basically, available means DB is available all the time as per CAP theorem.
* Soft state means even without input the system state may change.
* Eventual consistency implies that the system will become consistent over time.

## **Types of NoSQL databases :**

* **Document databases** Store data in documents similar to JSON objects. Each document contains pairs of fields and values. The values can be of various types including things like strings, numbers, booleans, arrays, or objects.
* **Key-value databases** Are a simpler type of database where each item contains keys and values.
* **Wide-column** Stores data in tables, rows, and dynamic columns.
* **Graph databases** Store data in nodes and edges. Nodes typically store information, while edges store information about the relationships between the nodes.

![scaling out image](images/types.png "scaling out image")

## **When should NoSQL be used?**

One or more of the following factors lead them to select a NoSQL database:

* Fast-paced Agile development
* Storage of structured and semi-structured data
* Huge volumes of data
* Requirements for scale-out architecture
* Modern application paradigms like microservices and real-time streaming

## **List of noSQL database :**

### **1. MongoDB**

MongoDB is an object-oriented, simple, dynamic, and scalable NoSQL database. It is based on the NoSQL document store model. The data objects are stored as separate documents inside a collection instead of storing the data in the columns and rows of a traditional relational database. MongoDB uses JSON-like documents with schemas.

**MongoDB Advantages and Use Cases :**

* It is a schema-less database and stores data as JSON-like documents. It offers flexibility and agility in terms of the type of records that can be stored and even the fields can be different from one document to another.
* Additionally, embedded documents provide faster queries through indexes and vastly reduce the I/O overload generally associated with database systems.
* MongoDB also provides several enterprise features, like high availability and horizontal scalability.
* MongoDB also offers support for several storage engines, ensuring that you can fine-tune your database based on the workload it is serving.
* Some of the most common use cases
  * Real-time view of your data
  * Mobile applications, IoT applications
  * Content management systems.

**MongoDB Disadvantages :**

* Management operations like patching are manual and time-consuming processes.
* MapReduce implementations still remain a slow process
* MongoDB also suffers from memory hog issues as the databases start scaling.

### **2. Hbase**

HBase is a column-oriented non-relational database management system that runs on top of the Hadoop Distributed File System (HDFS). HBase provides a fault-tolerant way of storing sparse data sets, which are common in many big data use cases. It is well suited for real-time data processing or random read/write access to large volumes of data.

**HBase Advantages and Use Cases :**

* HDFS is used as the distributed file system in HBase.
* This allows the database to store large data sets, even billions of rows
* Assures that the solution is very cost-effective when the data is scaled to gigabytes or petabytes.
* Failover support includes automatic recovery.
* Its close integration with Hadoop projects and MapReduce makes it an enticing solution for Hadoop distributions.
* Some of HBase’s common use cases
  * Online log analytics
  * Hadoop distributions
  * Write-heavy applications
  * Applications in need of large volumes (like Tweets, Facebook posts, etc.).

**HBase Disadvantages :**

* A master-slave architecture is used. This proves to be a single point of failure
* HBase does not have a query language. This means that to achieve SQL-like capabilities, one must use the JRuby-based HBase shell and technologies like Apache Hive
* The major problem with this approach is the high latency and steep learning curve in employing these technologies.
* It has some high hardware requirements and also high running and maintenance costs.

### **3. Cassandra**
  
Apache Cassandra is a column-family database and was originally developed by Facebook. Some of its key features include de-centralization to reduce failures and data replication to increase fault tolerance.

**Cassandra Advantages and Use Cases** :

* Cassandra uses a masterless "ring" architecture, which has benefits over legacy architectures.
* Although Cassandra stores data in columns and rows like a traditional RDBMS, it provides agility in the sense that it allows rows to have different columns. It also allows a change in the format of the columns.
* Cassandra offers advanced repair processes for read, write, and entropy (data consistency)
* This makes the cluster highly available and reliable.
* Lack of a single point of failure
* Better fault tolerance compared to document stores like MongoDB
* Some of Cassandra’s most common use cases
  * Messaging systems (for its superior read and write performance)
  * Real-time sensor data
  * E-commerce websites.

**Cassandra Disadvantages** :

* As the architecture is distributed, replicas can become inconsistent.
* If a node in a cluster shuts down, its coordinator node tries to preserve the data in the form of hints. When the failed node is brought online, the coordinator node hands off the hints to aid in the repair. This, however, can prove to be a burden for the coordinator node and overload it. As a result, you will see a loss of data replicas and refusal of writes from the coordinator node.
* While scanning data, Cassandra handles itself well if the primary key is known, but suffers severely if it is not. This is because it has to scan all its nodes in the cluster, resulting in high read time penalties.

### **4. Redis**

Redis is an open-source database that supports many different data structures such as strings, lists, hashes, sets, sorted sets, etc. It is written in ANSI C and it can be used with almost all of the programming languages and Linux and OS X operating systems.

**REDIS Advantages and Use Cases** :

* Works with an in-memory dataset to preserve its extremely fast performance and the implementation
* Uses a fork system call to create a duplicate of the current process with the data. This is so that the parent process can continue its operations with the existing clients and the child process can create a data copy on the disk.
* Some of REDIS most common use cases
  * Gaming
  * Ad-tech
  * Healthcare
  * Internet of things (IoT)
  * Financial services

**REDIS Disadvantages** :

* Requires at least 3 Masters with 2 slaves each to set up architecture.
* Since Data is sharded based on the hash-slots assigned to each Master. If the master holding some slots is down, data to be written to that slot will be lost.
* Clients connecting to the Redis cluster should be aware of the cluster topology, causing overhead configuration on Clients.
* Failover does not happen unless the master has at least one slave
* Slave promotion as a Master takes at least 30–50 seconds, so data written to cluster in that time window will be lost.

### **5. RavenDB**

RavenDB is a NoSQL document database that provides the benefits of a NoSQL database with all the conveniences of a relational database. It also offers fully transactional (ACID) data integrity so you can use it along with your existing SQL databases to get the most out of both types.

**RavenDB Advantages and Use Cases** :

* Highly scalable and it can create new nodes to keep up with increasing data traffic.
* RavenDB is available for installation on-premises as well as in the form of a cloud service provided by Amazon Web Services, Azure, etc.
* Offers features like Reduce, Map, and Index.
* Default JSON format storage.
* Designed to fit integrated testing.
* Automatically generates IDs for records.
* Some of RavenDB's most common use cases.
  * Small Businesses.
  * Computer Software industry.

**RavenDB Disadvantages** :

* It doesn't have an option to test sharding, replication, or authenticated access without purchasing the license.
* Licenses are a must for anything other than development (even UAT requires licenses).
* Designing solutions is difficult if the programmers are not well versed with the concept of an "eventually consistent" model.

## **SQL vs NoSQL Performance**

### **Unstructured Data**

**Development Speed** Before entering data into a SQL database, we need to define our schema (table) with a list of columns, their types, etc. This is a time-consuming process. In addition, any changes to the table, such as adding a column or changing an existing column, take longer because we have to specify a schema. In NoSQL databases, however, we do not have to specify a schema for storing and retrieving data. In addition, if the data changes due to business requirements, you can just keep working ahead and store the data in the updated format. This is without having to restructure your schema. As a result, NoSQL is more performant. Since the data is unstructured and unverified before it enters the database, it is possible to insert or save incorrect data. One way is to create a data validation code.

### **Write Speed**

We have seen that NoSQL databases don't require a fixed schema for entering and retrieving data. This also gives an advantage to some of the NoSQL databases: write (create/update) speed. In SQL databases, the inserted data is validated against the table's schema. Validating each data item against the corresponding column takes time. As a result, NoSQL databases provide a considerably higher number of write operations per second than SQL databases.

### **Indexes and Reading Data**

NoSQL databases are slower at efficiently reading large volumes of data from the database. In cases where multiple read operations per second are performed, a traditional SQL database can be the right choice.

**Indexes** However, NoSQL having a slower reading speed does not make it a loser. Since some of the NoSQL databases do not require indexing it manages memory much more efficiently than an SQL database. Also, we do not need an index to match the relative data as we do in SQL databases. Instead, we store all the related data as objects, which takes less time.

### **Use Cases**

As we have seen, both SQL and NoSQL databases have their respective advantages and places in the industry. Ultimately, it is about what the organization's needs are and what its future possibilities are. A traditional SQL database is a wise choice if you are looking for a long-used, stable technology. If you are looking to store large volumes of unstructured data as fast as possible, then NoSQL has to be your choice. If you want to read a lot of structured data, then SQL databases will be better. Lastly, if development speed is critical for your application NoSQL is the way to go.

## **Advantages of NoSQL:**

* Big Data Capability
* No Single Point of Failure
* Easy Replication
* No Need for Separate Caching Layer
* It provides fast performance and horizontal scalability.
* Can handle structured, semi-structured, and unstructured data with equal effect
* Simpler to implement than using RDBMS
* Excels at distributed database and multi-data center operations
* Offers a flexible schema design that can easily be altered without downtime or service disruption
* Compatibility with cheap commodity hardware clusters as transaction and data volumes increase, allowing you to process and store more data at a lower cost
* Support for auto-sharding, allowing NoSQL databases to natively and automatically spread data across an arbitrary number of servers, without needing
* The application should be aware of the server pool composition

## **Disadvantages of NoSQL Databases:**

* No standardization rules
* Limited query capabilities
* It does not offer any traditional database capabilities, like consistency when multiple transactions are performed simultaneously.
* When the volume of data increases it is difficult to maintain unique values as keys become difficult
* Doesn’t work as well with relational data
* The learning curve is stiff for novice developers
* NoSQL databases don’t offer the same reliability functions
* Associated with Relational Databases.
* Does not provide ACID properties by default

## **Conclusion:**

We can decide on the possibility of scaling and enhancing the performance of the project by considering the various topics discussed in the paper. This is because all the topics discussed play a major role in the selection of databases by use-case and future requirements and expansion.

## **Summary:**

* We have studied the difference between SQL and no SQL database
* How NoSQL databases are structured and formed.
* Why and when to use NoSQL databases.
* We have studied the features of the NoSQL database.
* On what principle are NoSQL databases designed
* Various NoSQL databases and their types
* SQL vs NoSQL performance comparison
* Advantages and Disadvantages of NoSQL databases

## **Reference:**

1. [Mongodb,NoSQL vs SQL Databases](https://www.mongodb.com/nosql-explained/nosql-vs-sql)

2. [Mongodb,What is NoSQL?](https://www.mongodb.com/nosql-explained)

3. [apache](https://hbase.apache.org/)

4. [cassandra](https://cassandra.apache.org/_/index.html)

5. [Redis](https://redis.io/s)

6. [ravendb](https://ravendb.net/)

7. [researchgate (reserchpaper)](https://www.researchgate.net/publication/261079289_A_performance_comparison_of_SQL_and_NoSQL_databases)

8. [Performance comparison of different NoSQL structure orientations (reserchpaper)](https://www.diva-portal.org/smash/get/diva2:1458321/FULLTEXT02)

9. [rochester (blogs and website)](https://www.cs.rochester.edu/courses/261/fall2017/termpaper/submissions/06/Paper.pdf)

10. [logz (blogs and website)](https://logz.io/blog/nosql-database-comparison/)

11. [guru99 (blogs and website)](https://www.guru99.com/nosql-tutorial.html#2)

12. [scalyr (blogs and website)](https://www.scalyr.com/blog/sql-vs-nosql-performance/)
